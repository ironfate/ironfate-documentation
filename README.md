The `ironfate-documentation` is Latex project to compose bilingual technical and catering riders plus band info. Simply put: everything you need as an event manager
or booking agent. The following instructions allow you to modify or contribute to the Iron Fate band documentation with Latex.

# Prerequisites

1. To install all necessary Latex libraries and compilation environment download and install a complete Texlive for your platform [here](https://www.tug.org/texlive/). This requires a lengthy time period for approx. 2 GiB. We recommend to use the online installer.
2. Download and install Texstudio for your platform [here](https://www.texstudio.org/).
3. If you're new to Latex use [this](https://en.wikibooks.org/wiki/LaTeX) wikibook as a good starting point for your seep learning curve.
4. Clone this repository and start your work.
---

# Project Structure

Here some words on our project structure.

```
ironfate-documentation
├── ironfate_docs.tex
├── doc
├── img
├── macros
│   └── macros.tex
├── packages
│   └── packages.tex
├── README.md
├── references
│   └── references.bib
├── sections
│   ├── bandinfo.tex
│   ├── caterider.tex
│   └── techrider.tex
├── template
│   └── title.tex
```

1. `ironfate_docs.tex` is the root document from where all packages, macros, and sections are included. Presumably, you don't have to do very much here. But you have to introduce this file once to Texstudio as root document before compliation.
2. Within folder `docs` there are additional latex documentations needed for special packages. Add further, rather exotic, documenations as pdf here.
3. Folder `img`is for all kinds of illustrations used within the documentation. Try to convert your graphics into pdf files if possible.
4. Folder `macros`just contains the file `macros.tex`. Add further macros to this file or change existing variable settings here.
5. Folder `packages` analogously but for package inclusion and configuration. Add new, required packages or change existing settings here.
6. `README.md` is this file. Do not forget potential project maintenance instructions here.
7. Folder `references` with file `references.bib` is currently not in use but should be used for literature references if needed, e.g., some handbooks on which the docs might refer to.
8. Folder `sections` contains the actual content. Each section should be created as separate file, which is include via `ironfate_docs.tex`. Hint: Add your inclusion logically in Texstudio and let the IDE create potential files and subfolders.
9. Folder `template`contains template file such as the title page template. In case you create more customized stuff like this, keep in mind to add it here.

Now you got the whole picture. Good luck'n'look!

---